//
//  ViewController.swift
//  MeterSpot
//
//  Created by Jefferey Cisneros on 1/11/17.
//  Copyright © 2017 MeterSpot Inc. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
